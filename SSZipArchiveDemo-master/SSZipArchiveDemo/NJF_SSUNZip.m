//
//  NJF_SSUNZip.m
//  SSZipArchiveDemo
//
//  Created by niujf on 2018/11/5.
//  Copyright © 2018年 niujf. All rights reserved.
//

#import "NJF_SSUNZip.h"

@implementation NJF_SSUNZip

+ (void)clearCacheResourcePath{
    [[NSFileManager defaultManager] removeItemAtPath:[NJF_SSUNZip compressionZipPath:@""] error:nil];
}

+ (NSDictionary*)getImageZipUrl:(NSString*)imageZipUrl unZipUrl:(NSString *)unzipPath progressHandler:(void(^)(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total))unZipProgressHandler{
    @autoreleasepool {
        
            NSString *zipPath = imageZipUrl;
            if (!zipPath) {
                return NSDictionary.new;
            }
            if (!unzipPath) {
                return NSDictionary.new;
            }
            NSLog(@"要解压的路径%@",unzipPath);
            BOOL success = [SSZipArchive unzipFileAtPath:zipPath
                                           toDestination:unzipPath
                                      preserveAttributes:YES
                                               overwrite:YES
                                          nestedZipLevel:0
                                                password:nil
                                                   error:nil
                                                delegate:nil
                                         progressHandler:^(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total) {
                                
                                if(unZipProgressHandler){
                            unZipProgressHandler(entry,zipInfo,entryNumber,entryNumber);
                                }
                
                            } completionHandler:^(NSString * _Nonnull path, BOOL succeeded, NSError * _Nullable error) {
                                
                            }];
        
            if (success) {
                NSLog(@"Success unzip");
               [[NSFileManager defaultManager] removeItemAtPath:imageZipUrl error:nil];
            } else {
                NSLog(@"No success unzip");
                return NSDictionary.new;
            }
        NSDictionary *items = [NJF_SSUNZip localFile:unzipPath];
        
            NSLog(@"%@",items);
  
        return items;
    }
}
//路径下面文件
+(NSMutableDictionary *)localFile:(NSString*)locaUrl {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSDirectoryEnumerator *dirEnum = [fm enumeratorAtPath:locaUrl];
    NSString *fileName;
    NSMutableDictionary *R = [NSMutableDictionary dictionary];
    while (fileName = [dirEnum nextObject]) {
        NSLog(@"短路径:%@", fileName);
        NSLog(@"全路径:%@", [locaUrl stringByAppendingPathComponent:fileName]);
        if ([fileName hasSuffix:@".png"]) {
            NSString *key = [fileName componentsSeparatedByString:@"/"].lastObject;
            [R setObject:[locaUrl stringByAppendingPathComponent:fileName] forKey:key];
        }
    }
    return R;
}

// 下载zip地址
/// @param zipUrl 资源下载地址
/// @param unZipFileName  解压资源文件名称地址
/// @param callback  文件路径字典
/// @param downloadProgress 下载进度
/// @param unZipProgressHandler 解压进度
+(void)downloadZipUrl:(NSString*)zipUrl unZipFileName:(NSString*)unZipFileName callback:(void (^)(NSDictionary *urlDic))callback  downloadProgress:(void(^)(NSProgress * _Nonnull downloadProgress))downloadProgressNew  progressHandler:(void(^)(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total))unZipProgressHandler{
    
    
    NSString *targetPathself = [NSString stringWithFormat:@"%@/%@.zip", [NJF_SSUNZip tempUnzipPath], @"temporaryZip"];
              [[NSFileManager defaultManager] removeItemAtPath:targetPathself error:nil];
    
       AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            //
                      /* 下载地址url */
                      NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:zipUrl]];
                      
                      /* 开始请求下载 */
                      NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
                          NSLog(@"下载进度：%.0f％", downloadProgress.fractionCompleted * 100)
                          ;
                          if(downloadProgressNew){
                              downloadProgressNew(downloadProgress);
                          }
                          
                      } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
                          /* 设定下载到的位置 */
                          return [NSURL fileURLWithPath:targetPathself];
                                  
                      }completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
                          //成功

                          if (callback) {
                              callback([NJF_SSUNZip getImageZipUrl:targetPathself unZipUrl:[NJF_SSUNZip compressionZipPath:unZipFileName] progressHandler:^(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total) {
                                    
                                  if(unZipProgressHandler){
                                      unZipProgressHandler(entry,zipInfo,entryNumber,entryNumber);
                                  }
                                  
                              }]);
                          }
                      }];
                       [downloadTask resume];
    

}

//压缩包临时路径
+(NSString *)tempUnzipPath {
    
    NSString *path = [NSString stringWithFormat:@"%@/\%@",
                      NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0],
                      [NSUUID UUID].UUIDString];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    [[NSFileManager defaultManager] createDirectoryAtURL:url
                             withIntermediateDirectories:YES
                                              attributes:nil
                                                   error:&error];
    if (error) {
        return nil;
    }
    return url.path;
}

//解压资源路径
+(NSString *)compressionZipPath:(NSString*)fileName{
    NSString *path = [NSString stringWithFormat:@"%@/\%@/\%@",
                      NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0],
                      @"ResourceFile",fileName];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    [[NSFileManager defaultManager] createDirectoryAtURL:url
                             withIntermediateDirectories:YES
                                              attributes:nil
                                                   error:&error];
    if (error) {
        return nil;
    }
    return url.path;
}



@end
