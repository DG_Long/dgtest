//
//  NJF_SSUNZip.h
//  SSZipArchiveDemo
//
//  Created by niujf on 2018/11/5.
//  Copyright © 2018年 niujf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SSZipArchive/SSZipArchive.h"
#import "AFNetworking/AFNetworking.h"

@interface NJF_SSUNZip : NSObject
/// 清楚所有缓存
+ (void)clearCacheResourcePath;
// 下载zip地址
/// @param zipUrl 资源下载地址
/// @param unZipFileName  解压资源文件名称地址
/// @param callback  文件路径字典
/// @param downloadProgressNew 下载进度
/// @param unZipProgressHandler 解压进度
+(void)downloadZipUrl:(NSString*_Nullable)zipUrl unZipFileName:(NSString*_Nullable)unZipFileName callback:(void (^_Nullable)(NSDictionary * _Nullable urlDic))callback  downloadProgress:(void(^_Nullable)(NSProgress * _Nonnull downloadProgress))downloadProgressNew  progressHandler:(void(^_Nullable)(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total))unZipProgressHandler;
@end

